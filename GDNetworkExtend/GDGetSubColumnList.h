//
//  GDSubColumnList.h
//  GDNetworkLayer
//
//  Created by hexuan on 2017/6/4.
//  Copyright © 2017年 hexuan. All rights reserved.
//

#import "GDRequestSender.h"
#import "GDBaseRequest.h"

@interface GDGetSubColumnList : GDRequestSender

@property (nonatomic,copy)NSArray *subColumnInfos;

@end
