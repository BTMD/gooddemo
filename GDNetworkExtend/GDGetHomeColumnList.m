//
//  GDGetHomeColumnList.m
//  GDNetworkLayer
//
//  Created by hexuan on 2017/6/3.
//  Copyright © 2017年 hexuan. All rights reserved.
//

#import "GDGetHomeColumnList.h"
#import "GDBaseRequest.h"

@implementation GDHomeColumn


@end

@interface GDHomeColumnRequest : GDBaseRequest

@end

@implementation GDHomeColumnRequest


@end



@implementation GDGetHomeColumnList

+ (Class)requestClass {
    return [GDHomeColumnRequest class];
}

+ (NSString *)path {
    return @"getHomeColumnList.php";
}

- (Class)arrValueClassForProKey:(NSString *)proKey {
    if ([proKey isEqualToString:@"columnInfos"]) {
        return [GDHomeColumn class];
    }
    return nil;
}

@end
