//
//  GDGetHomeColumnList.h
//  GDNetworkLayer
//
//  Created by hexuan on 2017/6/3.
//  Copyright © 2017年 hexuan. All rights reserved.
//

#import "GDRequestSender.h"

@interface GDHomeColumn : GDEntity

@property (nonatomic, copy) NSString *columnID;
@property (nonatomic, copy) NSString *columnName;
@property (nonatomic, copy) NSString *showFlg;
@property (nonatomic, copy) NSString *columnType;
@property (nonatomic, copy) NSString *sortInx;

@end

@interface GDGetHomeColumnList : GDRequestSender

@property (nonatomic,copy)NSArray *columnInfos;

@end
