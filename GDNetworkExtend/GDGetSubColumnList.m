//
//  GDSubColumnList.m
//  GDNetworkLayer
//
//  Created by hexuan on 2017/6/4.
//  Copyright © 2017年 hexuan. All rights reserved.
//

#import "GDGetSubColumnList.h"

@interface GDSubColumn : GDEntity

@property (nonatomic,copy)NSString *subColumnID;
@property (nonatomic,copy)NSString *subColumnType;
@property (nonatomic,copy)NSString *sortInx;
@property (nonatomic,copy)NSString *imageUrl;
@property (nonatomic,copy)NSString *subColumnName;
@property (nonatomic,copy)NSString *showFlg;


@end

@implementation GDSubColumn


@end

@interface GDSubColumnListRequest : GDBaseRequest

@end

@implementation GDSubColumnListRequest

@end


@implementation GDGetSubColumnList

+ (Class)requestClass {
    return [GDSubColumnListRequest class];
}

+ (NSString *)path {
    return @"getSubColumnList.php";
}

- (Class)arrValueClassForProKey:(NSString *)proKey {
    if ([proKey isEqualToString:@"subColumnInfos"]) {
        return [GDSubColumn class];
    }
    return nil;
}

@end
