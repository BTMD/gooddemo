//
//  GDMainViewController.m
//  AppTemplate_iOS
//
//  Created by hexuan on 2017/6/5.
//  Copyright © 2017年 hexuan. All rights reserved.
//

#import "GDMainViewController.h"
#import "GDGetSubColumnList.h"
#import "GDGetHomeColumnList.h"
#import "UIView+EX.h"
#import "APPConst.h"

#define kSelectedColor [UIColor colorWithRed:251/255.0 green:113/255.0 blue:0 alpha:1]

@interface GDMainViewController ()

@end

@implementation GDMainViewController {
    GDGetSubColumnList *getSubColumnList;
    GDGetHomeColumnList *getHomeColumnList;
    UIImageView *groupBtn;
    UIButton *selectedColumnBtn;
    UIView *columnBtnBottomView;
}

- (instancetype)init {
    if(self = [super init]) {
        getSubColumnList = [GDGetSubColumnList new];
        getHomeColumnList = [GDGetHomeColumnList new];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:243/255.0 green:244/255.0 blue:246/255.0 alpha:1];
    
    CGFloat offsetTop = kAppStatusBarHeight;
    
    UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
    logoView.frame = CGRectMake(0, offsetTop, 121, 37);
    [self.view addSubview:logoView];
    
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(logoView.rightX, offsetTop, kScreenWidth - logoView.width, logoView.height)];
    searchBar.placeholder = @"小别离";
    searchBar.searchBarStyle = UISearchBarStyleMinimal;
    searchBar.barTintColor = [UIColor whiteColor];
    [self.view addSubview:searchBar];
    
    UITextField *searchField = [searchBar valueForKey:@"searchField"];
    if (searchField) {
        searchField.layer.cornerRadius = 14.0f;
        searchField.layer.masksToBounds = YES;
    }
    
    offsetTop = searchBar.bottomY;
    
    groupBtn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"button_group.png"]];
    groupBtn.frame = CGRectMake(kScreenWidth - 144, offsetTop, 144, 37);
    [self.view addSubview:groupBtn];
    
    [getHomeColumnList sendRequestWithSuccess:^{
        CGFloat btnWidth = (kScreenWidth - groupBtn.width) / getHomeColumnList.columnInfos.count;
        int index = 0;
        for (GDHomeColumn *column in getHomeColumnList.columnInfos) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setTitle:column.columnName forState:UIControlStateNormal];
            [button addTarget:self action:@selector(switchHomeColumn:) forControlEvents:UIControlEventTouchUpInside];
            [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            button.frame = CGRectMake(btnWidth * index, groupBtn.y, btnWidth, groupBtn.height);
            [self.view addSubview:button];
            
            if (index == 0) {
                selectedColumnBtn = button;
                [selectedColumnBtn setTitleColor:kSelectedColor forState:UIControlStateNormal];
            }
            
            index++;
        }
        
        columnBtnBottomView = [[UIView alloc] initWithFrame:CGRectMake(0, groupBtn.bottomY, btnWidth, 4)];
        columnBtnBottomView.backgroundColor = kSelectedColor;
        [self.view addSubview:columnBtnBottomView];
    } failure:^(NSString *errorMsg) {
        
    }];
    
    [getSubColumnList sendRequestWithSuccess:^{
        //NSLog(@"%@", getSubColumnList.subColumnInfos);
    } failure:^(NSString *errorMsg) {
        
    }];
    
    // Do any additional setup after loading the view.
}

- (void)switchHomeColumn:(UIButton *)sender {
    [sender setTitleColor:kSelectedColor forState:UIControlStateNormal];
    [selectedColumnBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    selectedColumnBtn = sender;
    [UIView animateWithDuration:0.2 animations:^{
        columnBtnBottomView.x = sender.x;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
