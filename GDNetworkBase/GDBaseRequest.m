//
//  GDBaseRequest.m
//  GDNetworkLayer
//
//  Created by hexuan on 2017/6/1.
//  Copyright © 2017年 hexuan. All rights reserved.
//

#import "GDBaseRequest.h"
#import <objc/runtime.h>
#import <UIKit/UIKit.h>

@implementation GDBaseRequest

- (instancetype)init {
    if (self = [super init]) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            self.ptype = @"3";
        } else {
            self.ptype = @"4";
        }
        self.pserialNumber = @"LKJLKJSF23424323423423KKLSKDF";
        self.puser = @"freeuser";
    }
    return self;
}

- (NSDictionary *)toDictionary {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    unsigned int propsCount;
    
    
    Class current = [self class];
    while (current != [NSObject class]) {
        objc_property_t *props = class_copyPropertyList(current, &propsCount);
        for(int i = 0;i < propsCount; i++) {
            
            objc_property_t prop = props[i];
            NSString *propName = [NSString stringWithUTF8String:property_getName(prop)];
            id value = [self valueForKey:propName];
            if(value == nil) {
                
                value = [NSNull null];
            }
            [dic setObject:value forKey:propName];
        }
        free(props);
        current = [current superclass];
    }
    return dic;
}

@end
