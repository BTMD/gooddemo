//
//  GDEntity.h
//  GDNetworkLayer
//
//  Created by hexuan on 2017/6/3.
//  Copyright © 2017年 hexuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GDEntity : NSObject

- (void)updatePropertyWithDic:(NSDictionary *)dic;
- (void)processArrayValue:(NSArray *)value key:(NSString *)key;

// need to be override
- (Class)arrValueClassForProKey:(NSString *)proKey;

@end
