//
//  GDBaseRequest.h
//  GDNetworkLayer
//
//  Created by hexuan on 2017/6/1.
//  Copyright © 2017年 hexuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GDBaseRequest : NSObject

@property (nonatomic,copy) NSString *ptype;
@property (nonatomic,copy) NSString *plocation;
@property (nonatomic, copy) NSString *puser;
@property (nonatomic, copy) NSString *ptoken;
@property (nonatomic, copy) NSString *pversion;
@property (nonatomic, copy) NSString *pserverAddress;
@property (nonatomic, copy) NSString *pserialNumber;

- (NSDictionary *)toDictionary;

@end
