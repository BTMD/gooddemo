//
//  GDEntity.m
//  GDNetworkLayer
//
//  Created by hexuan on 2017/6/3.
//  Copyright © 2017年 hexuan. All rights reserved.
//

#import "GDEntity.h"

@implementation GDEntity

- (Class)arrValueClassForProKey:(NSString *)proKey {
    return nil;
}

+ (GDEntity *)instanceWithDic:(NSDictionary *)dic andClass:(Class)class {
    if (![dic isKindOfClass:[NSDictionary class]]) {
        NSLog(@"期望转成实体的类型不是字典");
        return nil;
    }
    else if(![class isSubclassOfClass:[GDEntity class]]){
        NSLog(@"实体必须是 %@类型", NSStringFromClass([GDEntity class]));
        return nil;
    }
    
    id obj = [[class alloc] init];
    for (NSString *key in [dic allKeys]) {
        id value = [dic objectForKey:key];
        if ([value isKindOfClass:[NSArray class]]) {
            [obj processArrayValue:value key:key];
        }
        else {
            [obj setValue:value forKey:key];[obj setValue:value forKey:key];
        }
    }
    
    return obj;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {

}

- (void)processArrayValue:(NSArray *)value key:(NSString *)key {
    if (key.length == 0 || value.count == 0) {
        return;
    }
    Class objClass = [self arrValueClassForProKey:key];
    if (objClass == nil) {
        NSLog(@"%@ %@ 数组成员类型未指定", self, key);
        return;
    }
    
    if (objClass == [NSString class]  ||
        objClass == [NSNumber class] ||
        objClass == [NSNull class]) {
        [self setValue:value forKey:key];
        return;
    }
    
    NSMutableArray *array = [NSMutableArray array];

    [self showNeedPropertyListWithDic:value[0] class:objClass];
    
    for (NSDictionary *dic in value) {
        id element = [GDEntity instanceWithDic:dic andClass:objClass];
        [array addObject:element];
    }
    [self setValue:array forKey:key];
}

- (void)updatePropertyWithDic:(NSDictionary *)dic {
    [self showNeedPropertyListWithDic:dic class:[self class]];
        for (NSString *key in [dic allKeys]) {
            id value = [dic objectForKey:key];
            if ([value isKindOfClass:[NSArray class]]) {
                [self processArrayValue:value key:key];
            }
            else {
                [self setValue:value forKey:key];
            }
        }
}

- (void)showNeedPropertyListWithDic:(NSDictionary *)dic class:(Class)class{
#ifdef DEBUG
    NSMutableString *promptStr = [NSMutableString stringWithFormat:@"\n%@ 需要如下属性请声明", NSStringFromClass(class)];
    BOOL hasNotExistProperty = NO;
    for (NSString *key in [dic allKeys]) {
        SEL keySEL = NSSelectorFromString(key);
        if (![class instancesRespondToSelector:keySEL]) {
            hasNotExistProperty = YES;
            id value = [dic objectForKey:key];
            if ([value isKindOfClass:[NSArray class]]){
                [promptStr appendFormat:@"\n@property (nonatomic,copy)NSArray *%@;", key];
            }
            else if ([value isKindOfClass:[NSString class]]) {
                [promptStr appendFormat:@"\n@property (nonatomic,copy)NSString *%@;", key];
            }
        }
    }
    if (hasNotExistProperty) {
        NSLog(@"%@", promptStr);
    }
#endif
}

@end
