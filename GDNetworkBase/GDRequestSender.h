//
//  GDRequestSender.h
//  GDNetworkLayer
//
//  Created by hexuan on 2017/6/2.
//  Copyright © 2017年 hexuan. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GDEntity.h"

typedef void (^success)(void);
typedef void (^failure)(NSString *errorMsg);

@interface GDRequestSender  : GDEntity

+ (Class) requestClass;
+ (NSString *)path;

- (void)sendRequestWithSuccess:(success)success failure:(failure)failure;

@end

