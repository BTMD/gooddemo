//
//  GDExtendRequest.h
//  GDNetworkLayer
//
//  Created by hexuan on 2017/6/2.
//  Copyright © 2017年 hexuan. All rights reserved.
//

#import "GDBaseRequest.h"

@interface GDPagingRequest : GDBaseRequest

@property (nonatomic, copy) NSString *start;
@property (nonatomic, copy) NSString *end;



@end
